//Group Members:
//Name: Ajaydeep Singh
//Id: C0744219
//Name: Amrik singh
//Id: C0742318
//Name: Preetwinder Kaur
//Id: C0743856

package com.example.droidcafewsettings;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.shadows.ShadowToast;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)


public class MainActivityTest {

    private MainActivity activity;

    // Get Order Activity View Object
    private View orderActivityView;


    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();

        orderActivityView = LayoutInflater.from(activity).inflate(R.layout.activity_order, null);
    }

    @Test
    public void activityIsNotNull() throws Exception {
        assertNotNull(activity);
    }

    @Test
    public void orderFrozenYogurtTest(){

        //1. Get Froyo ImageView button using id
        ImageView froyoBtn = activity.findViewById(R.id.froyo);
        //2. Check if the ImageView exists
        assertNotNull("Froyo Button cannot be found",froyoBtn);
        //3. Click the Froyo Image to add it to Orders
        froyoBtn.performClick();
        //4. Get the Toast using ShadowToast
        Toast toast = ShadowToast.getLatestToast();
        //5. Check that the Toast is not null
        assertNotNull("Toast is not null",toast);
        //5. Check if the toast text matches "You ordered a FroYo."
        Assert.assertEquals("You ordered a FroYo.", ShadowToast.getTextOfLatestToast());

    }


    @Test
    public void rememberUserInfoTest() {

        //1. Get Floating Shopping Cart button
        FloatingActionButton cartButton = activity.findViewById(R.id.fab);
        //2. Click the Cart Button
        cartButton.performClick();
        //2a. Check if new activity is started.
        Intent intent = Shadows.shadowOf(activity).peekNextStartedActivity();
        assertEquals(OrderActivity.class.getCanonicalName(), intent.getComponent().getClassName());

        //3. Get the EditText/Input Views
        //3a.Get the Name Edit Text View
        EditText textName = (EditText) orderActivityView.findViewById(R.id.name_text);
        System.out.println(textName.getText());
        textName.setText("fdghv");
        //3b. Get the Address Edit Text
        EditText textAddress = orderActivityView.findViewById(R.id.address_text);
        //3c. Get the Phone Edit Text
        EditText textPhone = orderActivityView.findViewById(R.id.phone_text);
        //3d. get the Note Edit text
        EditText textNote = orderActivityView.findViewById(R.id.note_text);

        //7. Give input to the Text fields
        //7a. Set/Give the input to Name Edit Text field.
        textName.setText("Ganesh Gaitonde");
        //7b. Set/Give the input to Address Edit Text field.
        textAddress.setText("Bombay");
        //7c. set/Give input to Phone Edit text field.
        textPhone.setText("6478549760");
        //7d. Set/Give input to Note Edit Text field.
        textNote.setText("Deliver at Home");

        //8. Get the Delivery Speed Radio Buttons
        RadioButton sameDayRb = orderActivityView.findViewById(R.id.sameday);
        RadioButton NextDayRb = orderActivityView.findViewById(R.id.nextday);
        RadioButton pickUpRb = orderActivityView.findViewById(R.id.pickup);

        //9. Set same Day Delivery Option to selected
        sameDayRb.setChecked(true);

        //10. Get the 'save Customer Info' Button
        Button saveinfoBtn = orderActivityView.findViewById(R.id.saveButton);
        //10a. Check if button is not null
        assertNotNull("Button is null",saveinfoBtn);
        //11. Click the save button
        saveinfoBtn.setPressed(true);
        saveinfoBtn.setPressed(false);
        //saveinfoBtn.performClick();

        //12. Press the System back button to go to previous activity.
        activity.onBackPressed();
        //activity.finish();

        //13. Click the Shopping cart Button Again on Main Activity
        cartButton.performClick();
        //14. Check if Order activity is started
        Intent orderActivityIntent = Shadows.shadowOf(activity).peekNextStartedActivity();
        assertEquals(OrderActivity.class.getCanonicalName(), orderActivityIntent.getComponent().getClassName());

        //*Note: App only remembers the user info i.e. Name, Address and Phone
        //15. Get the text(User Info) from Text edit fields.
        String name = textName.getText().toString();
        String address = textAddress.getText().toString();
        String phone = textPhone.getText().toString();

        //16. Check if the User Info. in Edit Text fields matches the info. input gave earlier.
        assertEquals("Ganesh Gaitonde",name);
        assertEquals("Bombay", address);
        assertEquals("6478549760", phone);

    }


}
